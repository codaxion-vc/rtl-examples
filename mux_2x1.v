
module mux ( a, b , sel, y);
input a;
input b;
input sel;
output y;

reg y ;

always  @ (a or b or sel)
begin
  y = 0;
  if (sel == 0) 
  begin
    y = a;
  end 
  else 
  begin
    y = b;
  end
end
endmodule
