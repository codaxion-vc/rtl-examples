`timescale 1ns/1ps
`include "1-bit-adder.bhv.v" 


module tb_one_bit_adder();

reg         a         ;
reg         b         ;
wire        sum       ;
wire        carry     ;

one_bit_adder uut (
    .a         (    a         ),
    .b         (    b         ),
    .sum       (    sum       ),
    .carry     (    carry     )
);

parameter PERIOD = 10;

initial begin
    $dumpfile("db_tb_one_bit_adder.vcd");
    $dumpvars(0, tb_one_bit_adder);
    #5 a=0 ;
    #5 b=0;
    #10 a = 1 ;
    #10 a = 0 ;
    #20 $finish;
end

endmodule
