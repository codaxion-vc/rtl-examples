/*   1 bit adder */

module one_bit_adder (a, b, sum, carry);
input  a ;
input  b ;
output sum ;
output carry ;

reg sum, carry ;

always @( a or b ) 
begin
   assign {sum,carry} = a + b ; 
end

endmodule

