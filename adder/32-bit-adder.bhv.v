module adder32( a, b, result );
   input[31:0] a;
   input[31:0] b;
   output [31:0] result;

   reg [31:0] result;
   reg [32:0] sum ;

   always @(a or b) 
   begin
   sum = {1'b0,a} + {1'b0,b};
   assign result = sum[32] ? sum[32:1]: sum[31:0];
   end
endmodule // adder32
