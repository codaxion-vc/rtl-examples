`timescale 1ns/1ps

module tb_mux;

reg         a      ;
reg         b      ;
reg         sel    ;
wire        y      ;

mux uut (
    .a      (    a      ),
    .b      (    b      ),
    .sel    (    sel    ),
    .y      (    y      )
);

parameter PERIOD = 10;

initial 
begin
    $dumpfile("db_tb_mux.vcd");
    $dumpvars(0, tb_mux);
    #5 a = 0 ;
    #5 b = 1 ;
    #5 sel = 0 ;
    #10 sel = 1 ;
    #15 $finish ; 
end

endmodule
